/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Abel
 */
@Entity
@Table(name = "HistorialDef1")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "HistorialDef1.findAll", query = "SELECT h FROM HistorialDef1 h"),
    @NamedQuery(name = "HistorialDef1.findById", query = "SELECT h FROM HistorialDef1 h WHERE h.id = :id"),
    @NamedQuery(name = "HistorialDef1.findByPalabra", query = "SELECT h FROM HistorialDef1 h WHERE h.palabra = :palabra"),
    @NamedQuery(name = "HistorialDef1.findByDefinicion", query = "SELECT h FROM HistorialDef1 h WHERE h.definicion = :definicion"),
    @NamedQuery(name = "HistorialDef1.findByFechaHora", query = "SELECT h FROM HistorialDef1 h WHERE h.fechaHora = :fechaHora")})
public class HistorialDef1 implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private Integer id;
    @Size(max = 2500)
    @Column(name = "palabra")
    private String palabra;
    @Size(max = 2500)
    @Column(name = "definicion")
    private String definicion;
    @Column(name = "fecha_hora")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaHora;

    public HistorialDef1() {
    }

    public HistorialDef1(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPalabra() {
        return palabra;
    }

    public void setPalabra(String palabra) {
        this.palabra = palabra;
    }

    public String getDefinicion() {
        return definicion;
    }

    public void setDefinicion(String definicion) {
        this.definicion = definicion;
    }

    public Date getFechaHora() {
        return fechaHora;
    }

    public void setFechaHora(Date fechaHora) {
        this.fechaHora = fechaHora;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof HistorialDef1)) {
            return false;
        }
        HistorialDef1 other = (HistorialDef1) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "root.entity.HistorialDef1[ id=" + id + " ]";
    }
    
}
