/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import root.controller.Controller;
import root.dao.HistorialDef1JpaController;
import root.dto.PalabraDTO;

/**
 *
 * @author Abel
 */
@Path("definiciones")
public class HistorialDef implements Serializable {
    
    public HistorialDef(){
        
    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listarDefiniciones(){
        
        HistorialDef1JpaController dao = new HistorialDef1JpaController();
        List<root.entity.HistorialDef1> lista = dao.findHistorialDef1Entities();
        
        return Response.ok(200).entity(lista).build();
        
    }
    
    @POST
    @Path("/{palabra}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response nuevaDefinicion(@PathParam("palabra") String palabra){
        
        Client client = ClientBuilder.newClient();
        WebTarget miRecurso = client.target("https://od-api.oxforddictionaries.com/api/v2/entries/es/" + palabra);
        PalabraDTO retorno = miRecurso.request(MediaType.APPLICATION_JSON).header("app_id","8a6077d1").header("app_key","d9fc7dc3f7c002029015fbc4ac419b8a").get(PalabraDTO.class);

        String definicion = retorno.getResults().get(0).getLexicalEntries().get(0).getEntries().get(0).getSenses().get(0).getDefinitions().get(0);
        
        root.entity.HistorialDef1 historialEntity = new root.entity.HistorialDef1();
        
        SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date(System.currentTimeMillis());
        System.out.println("Super fecha y hora: " + formatter.format(date));
        Date fechahora = date;
        
        int min = 1;
        int max = 2147483645;
 
        historialEntity.setId((int)Math.floor(Math.random()*(max-min+1)+min));
        historialEntity.setPalabra(palabra);
        historialEntity.setDefinicion(definicion);
        historialEntity.setFechaHora(fechahora);
        
        
        HistorialDef1JpaController dao = new HistorialDef1JpaController();
        
        try {
            dao.create(historialEntity);
        } catch (Exception ex) {
            Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return Response.ok(200).entity(definicion).build();
        
    }
    
}
