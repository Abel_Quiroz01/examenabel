/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.dto;

import java.util.List;

/**
 *
 * @author Abel
 */
public class EntryDTO {
    private List<GrammaticalFeatureDTO> grammaticalFeatures;
    private List<NoteDTO> notes;
    private List<SenseDTO> senses;
    private List<VariantFormDTO> variantForms;

    /**
     * @return the grammaticalFeatures
     */
    public List<GrammaticalFeatureDTO> getGrammaticalFeatures() {
        return grammaticalFeatures;
    }

    /**
     * @return the notes
     */
    public List<NoteDTO> getNotes() {
        return notes;
    }

    /**
     * @return the senses
     */
    public List<SenseDTO> getSenses() {
        return senses;
    }

    /**
     * @return the variantForms
     */
    public List<VariantFormDTO> getVariantForms() {
        return variantForms;
    }

    /**
     * @param grammaticalFeatures the grammaticalFeatures to set
     */
    public void setGrammaticalFeatures(List<GrammaticalFeatureDTO> grammaticalFeatures) {
        this.grammaticalFeatures = grammaticalFeatures;
    }

    /**
     * @param notes the notes to set
     */
    public void setNotes(List<NoteDTO> notes) {
        this.notes = notes;
    }

    /**
     * @param senses the senses to set
     */
    public void setSenses(List<SenseDTO> senses) {
        this.senses = senses;
    }

    /**
     * @param variantForms the variantForms to set
     */
    public void setVariantForms(List<VariantFormDTO> variantForms) {
        this.variantForms = variantForms;
    }
    
    
}
