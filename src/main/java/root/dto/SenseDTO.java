/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.dto;

import java.util.List;

/**
 *
 * @author Abel
 */
public class SenseDTO {
    private List<String> definitions;
    private List<ExampleDTO> examples;
    private String id;

    /**
     * @return the definitions
     */
    public List<String> getDefinitions() {
        return definitions;
    }

    /**
     * @return the examples
     */
    public List<ExampleDTO> getExamples() {
        return examples;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param definitions the definitions to set
     */
    public void setDefinitions(List<String> definitions) {
        this.definitions = definitions;
    }

    /**
     * @param examples the examples to set
     */
    public void setExamples(List<ExampleDTO> examples) {
        this.examples = examples;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }
    
    
}
