/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.dto;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Abel
 */

// No puedo creer que me haya dado la flojera de desarmar todo el JSON de oxford
// En puros DTO, imagino que deben haber maneras mas faciles, pero estoy muy
// feliz de poder capturar el retorno de la api.
public class PalabraDTO {

    // Atributos
    private String id;
    private MetadataDTO metadata;
    private List<ResultDTO> results;
    private String word;
    
    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @return the metadata
     */
    public MetadataDTO getMetadata() {
        return metadata;
    }

    /**
     * @return the results
     */
    public List<ResultDTO> getResults() {
        return results;
    }

    /**
     * @return the word
     */
    public String getWord() {
        return word;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @param metadata the metadata to set
     */
    public void setMetadata(MetadataDTO metadata) {
        this.metadata = metadata;
    }

    /**
     * @param results the results to set
     */
    public void setResults(List<ResultDTO> results) {
        this.results = results;
    }

    /**
     * @param word the word to set
     */
    public void setWord(String word) {
        this.word = word;
    }

}