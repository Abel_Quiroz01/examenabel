/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.dto;

import java.util.List;

/**
 *
 * @author Abel
 */
public class ResultDTO {
    private String id;
    private String language;
    private List<LexicalEntryDTO> lexicalEntries;
    private String type;
    private String word;

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @return the language
     */
    public String getLanguage() {
        return language;
    }

    /**
     * @return the lexicalEntries
     */
    public List<LexicalEntryDTO> getLexicalEntries() {
        return lexicalEntries;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @return the word
     */
    public String getWord() {
        return word;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @param language the language to set
     */
    public void setLanguage(String language) {
        this.language = language;
    }

    /**
     * @param lexicalEntries the lexicalEntries to set
     */
    public void setLexicalEntries(List<LexicalEntryDTO> lexicalEntries) {
        this.lexicalEntries = lexicalEntries;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @param word the word to set
     */
    public void setWord(String word) {
        this.word = word;
    }
    
    
}
