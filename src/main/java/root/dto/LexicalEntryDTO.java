/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.dto;

import java.util.List;

/**
 *
 * @author Abel
 */
public class LexicalEntryDTO {
    private List<CompoundDTO> compounds;
    private List<DerivativeDTO> derivatives;
    private List<EntryDTO> entries;
    private String language;
    private LexicalCategoryDTO lexicalCategory;
    private String text;

    /**
     * @return the compounds
     */
    public List<CompoundDTO> getCompounds() {
        return compounds;
    }

    /**
     * @return the derivatives
     */
    public List<DerivativeDTO> getDerivatives() {
        return derivatives;
    }

    /**
     * @return the entries
     */
    public List<EntryDTO> getEntries() {
        return entries;
    }

    /**
     * @return the language
     */
    public String getLanguage() {
        return language;
    }

    /**
     * @return the lexicalCategory
     */
    public LexicalCategoryDTO getLexicalCategory() {
        return lexicalCategory;
    }

    /**
     * @return the text
     */
    public String getText() {
        return text;
    }

    /**
     * @param compounds the compounds to set
     */
    public void setCompounds(List<CompoundDTO> compounds) {
        this.compounds = compounds;
    }

    /**
     * @param derivatives the derivatives to set
     */
    public void setDerivatives(List<DerivativeDTO> derivatives) {
        this.derivatives = derivatives;
    }

    /**
     * @param entries the entries to set
     */
    public void setEntries(List<EntryDTO> entries) {
        this.entries = entries;
    }

    /**
     * @param language the language to set
     */
    public void setLanguage(String language) {
        this.language = language;
    }

    /**
     * @param lexicalCategory the lexicalCategory to set
     */
    public void setLexicalCategory(LexicalCategoryDTO lexicalCategory) {
        this.lexicalCategory = lexicalCategory;
    }

    /**
     * @param text the text to set
     */
    public void setText(String text) {
        this.text = text;
    }
    
    
}
