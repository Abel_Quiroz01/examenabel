package root.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.lang.Math.*;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import root.entity.HistorialDef1;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import root.dto.PalabraDTO;
import org.json.*;
import root.dao.HistorialDef1JpaController;

/**
 *
 * @author Abel
 */
@WebServlet(name = "miControlador", urlPatterns = {"/miControlador"})
public class Controller extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet miControlador</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet miControlador at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        if(request.getParameter("btn_consultar") != null){
            String palabra = request.getParameter("txt_palabra");

            Client client = ClientBuilder.newClient();

            WebTarget miRecurso = client.target("https://od-api.oxforddictionaries.com/api/v2/entries/es/" + palabra);

            PalabraDTO retorno = miRecurso.request(MediaType.APPLICATION_JSON).header("app_id","baa50a27").header("app_key","07eecafbc91d08108ba40e36d87fa68b").get(PalabraDTO.class);

            System.out.println("El retorno es: " + retorno.getResults().get(0).getLexicalEntries().get(0).getEntries().get(0).getSenses().get(0).getDefinitions().get(0));

            String definicion = retorno.getResults().get(0).getLexicalEntries().get(0).getEntries().get(0).getSenses().get(0).getDefinitions().get(0);

            HistorialDef1 historialEntity = new HistorialDef1();

            SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = new Date(System.currentTimeMillis());
            System.out.println("Super fecha y hora: " + formatter.format(date));
            Date fechahora = date;

            int min = 1;
            int max = 2147483645;

            historialEntity.setId((int)Math.floor(Math.random()*(max-min+1)+min));
            historialEntity.setPalabra(palabra);
            historialEntity.setDefinicion(definicion);
            historialEntity.setFechaHora(fechahora);

            System.out.println("setId: " + historialEntity.getId());
            System.out.println("setPalabra: " + historialEntity.getPalabra());
            System.out.println("setDefinicion: " + historialEntity.getDefinicion());
            System.out.println("setFechaHora: " + historialEntity.getFechaHora());

            HistorialDef1JpaController dao = new HistorialDef1JpaController();

            try {
                dao.create(historialEntity);
            } catch (Exception ex) {
                Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
            }
           
            request.setAttribute("definicion", definicion);

            request.getRequestDispatcher("definicion.jsp").forward(request, response);

            //processRequest(request, response);
        } else if (request.getParameter("btn_historial") != null){
            List <HistorialDef1> listaDefiniciones;
            EntityManagerFactory emf = Persistence.createEntityManagerFactory("my_persistence_unit");
            EntityManager em = emf.createEntityManager();
            try{
                CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
                cq.select(cq.from(HistorialDef1.class));
                Query q = em.createQuery(cq);

                listaDefiniciones = q.getResultList();
                
                mostrarLista(request, response, listaDefiniciones);
            } catch (IOException | ServletException ex) {
                System.out.println(ex.toString());
            } 
            finally {
                em.close();
            }
        }
        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    protected void mostrarLista(HttpServletRequest request, HttpServletResponse response, List<HistorialDef1> listaDefiniciones)
        throws ServletException, IOException{
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>OxfordAPI - Base de datos</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<div class=\"container-fluid\">\n" +
                        "            <div class=\"row\">\n" +
                        "                <div class=\"col\"></div>\n" +
                        "                <div class=\"col-10\" id=\"divPrincipal\">");
            out.println("<h4>Historial de búsquedas</h4>");
            out.println("<hr>");
            out.println("<table class='table table-bordered table-sm table-hover'>");
            out.println("<tr>");
            out.println("<thead>");
            out.println("<th>ID</th>");
            out.println("<th>PALABRA</th>");
            out.println("<th>DEFINICION</th>");
            out.println("<th>FECHA Y HORA</th>");
            out.println("</thead>");
            out.println("</tr>");
            for(int i = 0; i < listaDefiniciones.size(); i++){
                out.println("<tr>");
                out.println("<td>" + listaDefiniciones.get(i).getId() + "</td>");
                out.println("<td>" + listaDefiniciones.get(i).getPalabra() + "</td>");
                out.println("<td>" + listaDefiniciones.get(i).getDefinicion() + "</td>");
                out.println("<td>" + listaDefiniciones.get(i).getFechaHora()+ "</td>");
                out.println("</tr>");
            }
            out.println("</table>");
            out.println("<input type=\"button\" value=\"Volver\" class=\"btn btn-info btn-block\" onclick=\"window.history.back();\">");
            out.println("</div>\n" +
                        "                <div class=\"col\"></div>\n" +
                        "            </div>\n" +
                        "        </div>");
            out.println("</body>");
            out.println("</html>");
        }
    }
    
}
