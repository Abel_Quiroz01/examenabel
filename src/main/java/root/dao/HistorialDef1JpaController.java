/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.dao;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import root.dao.exceptions.NonexistentEntityException;
import root.dao.exceptions.PreexistingEntityException;
import root.entity.HistorialDef1;

/**
 *
 * @author Abel
 */
public class HistorialDef1JpaController implements Serializable {

    public HistorialDef1JpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("my_persistence_unit");

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public HistorialDef1JpaController() {
    }

    public void create(HistorialDef1 historialDef1) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(historialDef1);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findHistorialDef1(historialDef1.getId()) != null) {
                throw new PreexistingEntityException("HistorialDef1 " + historialDef1 + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(HistorialDef1 historialDef1) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            historialDef1 = em.merge(historialDef1);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = historialDef1.getId();
                if (findHistorialDef1(id) == null) {
                    throw new NonexistentEntityException("The historialDef1 with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            HistorialDef1 historialDef1;
            try {
                historialDef1 = em.getReference(HistorialDef1.class, id);
                historialDef1.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The historialDef1 with id " + id + " no longer exists.", enfe);
            }
            em.remove(historialDef1);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<HistorialDef1> findHistorialDef1Entities() {
        return findHistorialDef1Entities(true, -1, -1);
    }

    public List<HistorialDef1> findHistorialDef1Entities(int maxResults, int firstResult) {
        return findHistorialDef1Entities(false, maxResults, firstResult);
    }

    private List<HistorialDef1> findHistorialDef1Entities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(HistorialDef1.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public HistorialDef1 findHistorialDef1(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(HistorialDef1.class, id);
        } finally {
            em.close();
        }
    }

    public int getHistorialDef1Count() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<HistorialDef1> rt = cq.from(HistorialDef1.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
