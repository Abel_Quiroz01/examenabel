<%-- 
    Document   : definicion
    Created on : 07-05-2021, 3:57:58
    Author     : Abel
--%>

<%@page import="java.util.Iterator"%>
<%@page import="root.entity.HistorialDef1"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    String definicion = request.getAttribute("definicion").toString();
    //List<TblHistorialDefiniciones> tblHistorialDefiniciones = (List<TblHistorialDefiniciones>) request.getAttribute("tblHistorialDefiniciones");
    //Iterator<TblHistorialDefiniciones> iteradorDefiniciones = tblHistorialDefiniciones.iterator();
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Definición Resultado</title>
        
    </head>
    <body>
        <div class="container">
            <div class="row">                                                                              
                    <h6>Definición encontrada:</strong></h6>
                    <h6>Fuente: Oxford Dictionary</h6>
                    <p><%= definicion%></p>
                    <hr>
                    <input type="button" value="Volver" class="btn btn-info btn-block" onclick="window.history.back();">
                </div>
                <div class="col"></div>
            </div>
        </div>
        <p></p>
    </body>
</html>
