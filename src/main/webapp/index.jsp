<%-- 
    Document   : index
    Created on : 07-05-2021, 22:43:09
    Author     : Abel
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Diccionario</title>   
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col"></div>
                <div class="col-9" id="divPrincipal">   
                    <h6>Abel Quiroz - Aplicaciones Empresariales</h6>
                    <h6><strong>Endpoints API Rest</strong></h6><br>
                    <h6>GET: https://examenabel.herokuapp.com/api/definiciones</h6>
                    <h6>POST: https://examenabel.herokuapp.com/api/definiciones/{palabra}</h6>
                    <hr>
                    <form name="frm_consulta" action="miControlador" method="POST">
                        <div class="form-group row">
                            <label for="txt_palabra" class="col-sm-3 col-form-label">Ingrese una palabra:</label>
                            <div class="col-sm-9">
                                <input type="text" name="txt_palabra" class="form-control">                               
                            </div>
                        </div>
                        <input type="submit" name="btn_consultar" value="Buscar difinición palabra" class="btn btn-primary btn-block">
                        <input type="submit" name="btn_historial" value="historial" class="btn btn-success btn-block">
                    </form>
                </div>
                <div class="col"></div>
            </div>
        </div>
    </body>
</html>
